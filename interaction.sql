CREATE TABLE user_swipes (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_id BIGINT NOT NULL,
    mutual_id BIGINT NOT NULL,
    is_like BOOLEAN NOT NULL
);

ALTER TABLE user_swipes
ADD CONSTRAINT unique_user_mutual_id UNIQUE (user_id, mutual_id);

CREATE OR REPLACE FUNCTION update_updated_at_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = CURRENT_TIMESTAMP;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_user_swipes_updated_at
BEFORE UPDATE ON user_swipes
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();