## README for Dealls Date Apps - Interaction Service

### Application Description

Dealls Date Apps is a dating application designed with the following features:

- **Free User Swipes**: Users can view, swipe left (pass), and swipe right (like) up to 10 other dating profiles per day.
- **Profile Visibility**: The same profiles won’t appear twice in a single day.
- **Premium Packages**: Users can purchase premium packages (Silver, Gold, Platinum) that offer different maximum swipe limits per day.

### Interaction Service Description

The Interaction Service is responsible for managing user interactions such as viewing profiles and swiping left or right. It also tracks the daily swipe limits and ensures that users adhere to the rules.

### Technologies Used

- **Protocol Buffers (protobuf)**: For defining service interfaces and data structures.
- **gRPC**: For communication between microservices.
- **HTTP**: For external API endpoints.
- **JWT (JSON Web Tokens)**: For user authentication and secure access to endpoints.
- **PostgreSQL**: As the database for storing user information and package details.

### Running the Service

1. **Set up the environment**: Copy the `example.env` file to `.env` and configure the database host and other environment variables to match your local setup.
   `cp example.env .env`
2. Execute the `interaction.sql` file located in the root directory to set up the database.
3. Generate the protobuf files by running the script:
    ```bash generate.sh```
4. Install the necessary libraries: 
    ```go mod vendor```
5. Start the service:
    ```make run```
6. Once the service is running, you can test the API using Swagger at: `http://localhost:3026/api/interactions/docs`

### Running Unit Tests

- You can run unit tests for the project using the following command: `go test -v ./server/api_test`

Note :
The Interaction Service needs the Auth Service to run concurrently for full functionality.