package csvutils

import (
	"encoding/csv"
	"io"

	"github.com/gocarina/gocsv"
)

func SetCustomReader(separator rune) {
	gocsv.SetCSVReader(func(in io.Reader) gocsv.CSVReader {
		r := csv.NewReader(in)
		r.Comma = separator
		return r
	})
}

func SetCustomWriter(separator rune) {
	gocsv.SetCSVWriter(func(w io.Writer) *gocsv.SafeCSVWriter {
		wr := csv.NewWriter(w)
		wr.Comma = separator
		return gocsv.NewSafeCSVWriter(wr)
	})
}
