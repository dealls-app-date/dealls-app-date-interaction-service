package fileutils

import (
	"path/filepath"
	"strings"
)

func FileNameWithoutExtTrimSuffix(fileName string) string {
	return strings.TrimSuffix(fileName, filepath.Ext(fileName))
}
