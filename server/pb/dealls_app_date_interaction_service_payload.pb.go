// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.1
// 	protoc        v5.26.1
// source: dealls_app_date_interaction_service_payload.proto

package pb

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type PackageType int32

const (
	PackageType_SILVER   PackageType = 0
	PackageType_GOLD     PackageType = 1
	PackageType_PLATINUM PackageType = 2
)

// Enum value maps for PackageType.
var (
	PackageType_name = map[int32]string{
		0: "SILVER",
		1: "GOLD",
		2: "PLATINUM",
	}
	PackageType_value = map[string]int32{
		"SILVER":   0,
		"GOLD":     1,
		"PLATINUM": 2,
	}
)

func (x PackageType) Enum() *PackageType {
	p := new(PackageType)
	*p = x
	return p
}

func (x PackageType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (PackageType) Descriptor() protoreflect.EnumDescriptor {
	return file_dealls_app_date_interaction_service_payload_proto_enumTypes[0].Descriptor()
}

func (PackageType) Type() protoreflect.EnumType {
	return &file_dealls_app_date_interaction_service_payload_proto_enumTypes[0]
}

func (x PackageType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use PackageType.Descriptor instead.
func (PackageType) EnumDescriptor() ([]byte, []int) {
	return file_dealls_app_date_interaction_service_payload_proto_rawDescGZIP(), []int{0}
}

type Empty struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *Empty) Reset() {
	*x = Empty{}
	if protoimpl.UnsafeEnabled {
		mi := &file_dealls_app_date_interaction_service_payload_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Empty) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Empty) ProtoMessage() {}

func (x *Empty) ProtoReflect() protoreflect.Message {
	mi := &file_dealls_app_date_interaction_service_payload_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Empty.ProtoReflect.Descriptor instead.
func (*Empty) Descriptor() ([]byte, []int) {
	return file_dealls_app_date_interaction_service_payload_proto_rawDescGZIP(), []int{0}
}

type ErrorBodyResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Error   bool   `protobuf:"varint,1,opt,name=error,proto3" json:"error,omitempty"`
	Code    uint32 `protobuf:"varint,2,opt,name=code,proto3" json:"code,omitempty"`
	Message string `protobuf:"bytes,3,opt,name=message,proto3" json:"message,omitempty"`
}

func (x *ErrorBodyResponse) Reset() {
	*x = ErrorBodyResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_dealls_app_date_interaction_service_payload_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ErrorBodyResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ErrorBodyResponse) ProtoMessage() {}

func (x *ErrorBodyResponse) ProtoReflect() protoreflect.Message {
	mi := &file_dealls_app_date_interaction_service_payload_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ErrorBodyResponse.ProtoReflect.Descriptor instead.
func (*ErrorBodyResponse) Descriptor() ([]byte, []int) {
	return file_dealls_app_date_interaction_service_payload_proto_rawDescGZIP(), []int{1}
}

func (x *ErrorBodyResponse) GetError() bool {
	if x != nil {
		return x.Error
	}
	return false
}

func (x *ErrorBodyResponse) GetCode() uint32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *ErrorBodyResponse) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

type DefaultResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Error   bool   `protobuf:"varint,1,opt,name=error,proto3" json:"error,omitempty"`
	Code    uint32 `protobuf:"varint,2,opt,name=code,proto3" json:"code,omitempty"`
	Message string `protobuf:"bytes,3,opt,name=message,proto3" json:"message,omitempty"`
}

func (x *DefaultResponse) Reset() {
	*x = DefaultResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_dealls_app_date_interaction_service_payload_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DefaultResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DefaultResponse) ProtoMessage() {}

func (x *DefaultResponse) ProtoReflect() protoreflect.Message {
	mi := &file_dealls_app_date_interaction_service_payload_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DefaultResponse.ProtoReflect.Descriptor instead.
func (*DefaultResponse) Descriptor() ([]byte, []int) {
	return file_dealls_app_date_interaction_service_payload_proto_rawDescGZIP(), []int{2}
}

func (x *DefaultResponse) GetError() bool {
	if x != nil {
		return x.Error
	}
	return false
}

func (x *DefaultResponse) GetCode() uint32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *DefaultResponse) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

type UserData struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	UserId uint64 `protobuf:"varint,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	Name   string `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Bio    string `protobuf:"bytes,3,opt,name=bio,proto3" json:"bio,omitempty"`
	Gender string `protobuf:"bytes,4,opt,name=gender,proto3" json:"gender,omitempty"`
}

func (x *UserData) Reset() {
	*x = UserData{}
	if protoimpl.UnsafeEnabled {
		mi := &file_dealls_app_date_interaction_service_payload_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UserData) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UserData) ProtoMessage() {}

func (x *UserData) ProtoReflect() protoreflect.Message {
	mi := &file_dealls_app_date_interaction_service_payload_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UserData.ProtoReflect.Descriptor instead.
func (*UserData) Descriptor() ([]byte, []int) {
	return file_dealls_app_date_interaction_service_payload_proto_rawDescGZIP(), []int{3}
}

func (x *UserData) GetUserId() uint64 {
	if x != nil {
		return x.UserId
	}
	return 0
}

func (x *UserData) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *UserData) GetBio() string {
	if x != nil {
		return x.Bio
	}
	return ""
}

func (x *UserData) GetGender() string {
	if x != nil {
		return x.Gender
	}
	return ""
}

type GetSwipeProfileResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Error    bool        `protobuf:"varint,1,opt,name=error,proto3" json:"error,omitempty"`
	Code     uint32      `protobuf:"varint,2,opt,name=code,proto3" json:"code,omitempty"`
	Message  string      `protobuf:"bytes,3,opt,name=message,proto3" json:"message,omitempty"`
	UserData []*UserData `protobuf:"bytes,4,rep,name=user_data,json=userData,proto3" json:"user_data,omitempty"`
}

func (x *GetSwipeProfileResponse) Reset() {
	*x = GetSwipeProfileResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_dealls_app_date_interaction_service_payload_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetSwipeProfileResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetSwipeProfileResponse) ProtoMessage() {}

func (x *GetSwipeProfileResponse) ProtoReflect() protoreflect.Message {
	mi := &file_dealls_app_date_interaction_service_payload_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetSwipeProfileResponse.ProtoReflect.Descriptor instead.
func (*GetSwipeProfileResponse) Descriptor() ([]byte, []int) {
	return file_dealls_app_date_interaction_service_payload_proto_rawDescGZIP(), []int{4}
}

func (x *GetSwipeProfileResponse) GetError() bool {
	if x != nil {
		return x.Error
	}
	return false
}

func (x *GetSwipeProfileResponse) GetCode() uint32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *GetSwipeProfileResponse) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

func (x *GetSwipeProfileResponse) GetUserData() []*UserData {
	if x != nil {
		return x.UserData
	}
	return nil
}

type SwipeRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	UserId uint64 `protobuf:"varint,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	IsLike bool   `protobuf:"varint,2,opt,name=is_like,json=isLike,proto3" json:"is_like,omitempty"`
}

func (x *SwipeRequest) Reset() {
	*x = SwipeRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_dealls_app_date_interaction_service_payload_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SwipeRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SwipeRequest) ProtoMessage() {}

func (x *SwipeRequest) ProtoReflect() protoreflect.Message {
	mi := &file_dealls_app_date_interaction_service_payload_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SwipeRequest.ProtoReflect.Descriptor instead.
func (*SwipeRequest) Descriptor() ([]byte, []int) {
	return file_dealls_app_date_interaction_service_payload_proto_rawDescGZIP(), []int{5}
}

func (x *SwipeRequest) GetUserId() uint64 {
	if x != nil {
		return x.UserId
	}
	return 0
}

func (x *SwipeRequest) GetIsLike() bool {
	if x != nil {
		return x.IsLike
	}
	return false
}

var File_dealls_app_date_interaction_service_payload_proto protoreflect.FileDescriptor

var file_dealls_app_date_interaction_service_payload_proto_rawDesc = []byte{
	0x0a, 0x31, 0x64, 0x65, 0x61, 0x6c, 0x6c, 0x73, 0x5f, 0x61, 0x70, 0x70, 0x5f, 0x64, 0x61, 0x74,
	0x65, 0x5f, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x5f, 0x70, 0x61, 0x79, 0x6c, 0x6f, 0x61, 0x64, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x12, 0x2e, 0x64, 0x65, 0x61, 0x6c, 0x6c, 0x73, 0x5f, 0x61, 0x70, 0x70, 0x5f,
	0x64, 0x61, 0x74, 0x65, 0x5f, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2e, 0x76, 0x31, 0x22, 0x07, 0x0a, 0x05, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x57, 0x0a, 0x11,
	0x45, 0x72, 0x72, 0x6f, 0x72, 0x42, 0x6f, 0x64, 0x79, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x12, 0x14, 0x0a, 0x05, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08,
	0x52, 0x05, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d,
	0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65,
	0x73, 0x73, 0x61, 0x67, 0x65, 0x22, 0x55, 0x0a, 0x0f, 0x44, 0x65, 0x66, 0x61, 0x75, 0x6c, 0x74,
	0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x65, 0x72, 0x72, 0x6f,
	0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x05, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x12, 0x12,
	0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x04, 0x63, 0x6f,
	0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x22, 0x61, 0x0a, 0x08,
	0x55, 0x73, 0x65, 0x72, 0x44, 0x61, 0x74, 0x61, 0x12, 0x17, 0x0a, 0x07, 0x75, 0x73, 0x65, 0x72,
	0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x04, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49,
	0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x62, 0x69, 0x6f, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x03, 0x62, 0x69, 0x6f, 0x12, 0x16, 0x0a, 0x06, 0x67, 0x65, 0x6e, 0x64, 0x65,
	0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x67, 0x65, 0x6e, 0x64, 0x65, 0x72, 0x22,
	0xb4, 0x01, 0x0a, 0x17, 0x47, 0x65, 0x74, 0x53, 0x77, 0x69, 0x70, 0x65, 0x50, 0x72, 0x6f, 0x66,
	0x69, 0x6c, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x65,
	0x72, 0x72, 0x6f, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x05, 0x65, 0x72, 0x72, 0x6f,
	0x72, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0d, 0x52,
	0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x12,
	0x55, 0x0a, 0x09, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x64, 0x61, 0x74, 0x61, 0x18, 0x04, 0x20, 0x03,
	0x28, 0x0b, 0x32, 0x38, 0x2e, 0x64, 0x65, 0x61, 0x6c, 0x6c, 0x73, 0x5f, 0x61, 0x70, 0x70, 0x5f,
	0x64, 0x61, 0x74, 0x65, 0x5f, 0x69, 0x6e, 0x74, 0x65, 0x72, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e,
	0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2e, 0x76, 0x31, 0x2e, 0x55, 0x73, 0x65, 0x72, 0x44, 0x61, 0x74, 0x61, 0x52, 0x08, 0x75, 0x73,
	0x65, 0x72, 0x44, 0x61, 0x74, 0x61, 0x22, 0x40, 0x0a, 0x0c, 0x53, 0x77, 0x69, 0x70, 0x65, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x17, 0x0a, 0x07, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x04, 0x52, 0x06, 0x75, 0x73, 0x65, 0x72, 0x49, 0x64, 0x12,
	0x17, 0x0a, 0x07, 0x69, 0x73, 0x5f, 0x6c, 0x69, 0x6b, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x08,
	0x52, 0x06, 0x69, 0x73, 0x4c, 0x69, 0x6b, 0x65, 0x2a, 0x31, 0x0a, 0x0b, 0x50, 0x61, 0x63, 0x6b,
	0x61, 0x67, 0x65, 0x54, 0x79, 0x70, 0x65, 0x12, 0x0a, 0x0a, 0x06, 0x53, 0x49, 0x4c, 0x56, 0x45,
	0x52, 0x10, 0x00, 0x12, 0x08, 0x0a, 0x04, 0x47, 0x4f, 0x4c, 0x44, 0x10, 0x01, 0x12, 0x0c, 0x0a,
	0x08, 0x50, 0x4c, 0x41, 0x54, 0x49, 0x4e, 0x55, 0x4d, 0x10, 0x02, 0x42, 0x06, 0x5a, 0x04, 0x2e,
	0x2f, 0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_dealls_app_date_interaction_service_payload_proto_rawDescOnce sync.Once
	file_dealls_app_date_interaction_service_payload_proto_rawDescData = file_dealls_app_date_interaction_service_payload_proto_rawDesc
)

func file_dealls_app_date_interaction_service_payload_proto_rawDescGZIP() []byte {
	file_dealls_app_date_interaction_service_payload_proto_rawDescOnce.Do(func() {
		file_dealls_app_date_interaction_service_payload_proto_rawDescData = protoimpl.X.CompressGZIP(file_dealls_app_date_interaction_service_payload_proto_rawDescData)
	})
	return file_dealls_app_date_interaction_service_payload_proto_rawDescData
}

var file_dealls_app_date_interaction_service_payload_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_dealls_app_date_interaction_service_payload_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_dealls_app_date_interaction_service_payload_proto_goTypes = []interface{}{
	(PackageType)(0),                // 0: dealls_app_date_interaction_service.service.v1.PackageType
	(*Empty)(nil),                   // 1: dealls_app_date_interaction_service.service.v1.Empty
	(*ErrorBodyResponse)(nil),       // 2: dealls_app_date_interaction_service.service.v1.ErrorBodyResponse
	(*DefaultResponse)(nil),         // 3: dealls_app_date_interaction_service.service.v1.DefaultResponse
	(*UserData)(nil),                // 4: dealls_app_date_interaction_service.service.v1.UserData
	(*GetSwipeProfileResponse)(nil), // 5: dealls_app_date_interaction_service.service.v1.GetSwipeProfileResponse
	(*SwipeRequest)(nil),            // 6: dealls_app_date_interaction_service.service.v1.SwipeRequest
}
var file_dealls_app_date_interaction_service_payload_proto_depIdxs = []int32{
	4, // 0: dealls_app_date_interaction_service.service.v1.GetSwipeProfileResponse.user_data:type_name -> dealls_app_date_interaction_service.service.v1.UserData
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_dealls_app_date_interaction_service_payload_proto_init() }
func file_dealls_app_date_interaction_service_payload_proto_init() {
	if File_dealls_app_date_interaction_service_payload_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_dealls_app_date_interaction_service_payload_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Empty); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_dealls_app_date_interaction_service_payload_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ErrorBodyResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_dealls_app_date_interaction_service_payload_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DefaultResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_dealls_app_date_interaction_service_payload_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UserData); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_dealls_app_date_interaction_service_payload_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetSwipeProfileResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_dealls_app_date_interaction_service_payload_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SwipeRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_dealls_app_date_interaction_service_payload_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_dealls_app_date_interaction_service_payload_proto_goTypes,
		DependencyIndexes: file_dealls_app_date_interaction_service_payload_proto_depIdxs,
		EnumInfos:         file_dealls_app_date_interaction_service_payload_proto_enumTypes,
		MessageInfos:      file_dealls_app_date_interaction_service_payload_proto_msgTypes,
	}.Build()
	File_dealls_app_date_interaction_service_payload_proto = out.File
	file_dealls_app_date_interaction_service_payload_proto_rawDesc = nil
	file_dealls_app_date_interaction_service_payload_proto_goTypes = nil
	file_dealls_app_date_interaction_service_payload_proto_depIdxs = nil
}
