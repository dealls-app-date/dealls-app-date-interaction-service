package api

import (
	"context"
	authPB "dealls-app-date-interaction-service/server/lib/stubs/auth-service"
	pb "dealls-app-date-interaction-service/server/pb"
	"errors"
	"net/http"
	"strconv"

	"go.elastic.co/apm/v2"
)

func (s *Server) GetSwipeProfile(ctx context.Context, req *pb.Empty) (*pb.GetSwipeProfileResponse, error) {
	span, _ := apm.StartSpan(ctx, "GetSwipeProfile", "process")
	span.Action = "execute"
	defer span.End()

	logger := s.GetLogger()
	logger.Infof("[func: register] Check request %v", req)

	accessToken, err := s.manager.GetAccessToken(ctx)
	if err != nil {
		return nil, err
	}

	userData, err := s.manager.Verify(*accessToken)
	if err != nil {
		return nil, err
	}

	userId, err := strconv.ParseUint(userData.UserId, 10, 64)
	if err != nil {
		return nil, err
	}

	swipedToday, err := s.provider.GetSwipedMutualIdsToday(ctx, userId)
	if err != nil {
		return nil, err
	}

	authClient := s.GetSvcConn().AuthServiceClient()
	maxCountData, err := authClient.GetUserMaxCount(ctx, &authPB.DefaultRequest{
		UserId: userId,
	})
	if err != nil {
		return nil, err
	}

	limit := int(maxCountData.GetMaxCount()) - len(swipedToday)
	logger.Infof("len swiped todayç %v", len(swipedToday))
	logger.Infof("max count %v", maxCountData.GetMaxCount())
	logger.Infof("limit %v", limit)
	if limit <= 0 {
		limit = 0
	}

	var swipedTodayIDs []uint64
	for _, idPtr := range swipedToday {
		swipedTodayIDs = append(swipedTodayIDs, *idPtr)
	}
	swipedTodayIDs = append(swipedTodayIDs, userId)

	profileList, err := authClient.GetAvailableSwipeProfileList(ctx, &authPB.AvailableSwipeProfileListRequest{
		Limit:  uint64(limit),
		UserId: swipedTodayIDs,
	})
	if err != nil {
		return nil, err
	}

	var userDataList []*pb.UserData
	for _, userData := range profileList.UserData {
		userDataList = append(userDataList, &pb.UserData{
			UserId: userData.GetUserId(),
			Name:   userData.GetName(),
			Gender: userData.GetGender(),
			Bio:    userData.GetBio(),
		})
	}

	return &pb.GetSwipeProfileResponse{
		Error:    false,
		Code:     http.StatusOK,
		Message:  "Success",
		UserData: userDataList,
	}, nil
}

func (s *Server) Swipe(ctx context.Context, req *pb.SwipeRequest) (*pb.DefaultResponse, error) {
	span, _ := apm.StartSpan(ctx, "GetSwipeProfile", "process")
	span.Action = "execute"
	defer span.End()

	logger := s.GetLogger()
	logger.Infof("[func: register] Check request %v", req)

	accessToken, err := s.manager.GetAccessToken(ctx)
	if err != nil {
		return nil, err
	}

	userData, err := s.manager.Verify(*accessToken)
	if err != nil {
		return nil, err
	}

	userId, err := strconv.ParseUint(userData.UserId, 10, 64)
	if err != nil {
		return nil, err
	}

	if req.GetUserId() == 0 {
		return nil, errors.New("User id is required")
	}

	if userId == req.UserId {
		return nil, errors.New("Can't vote for yourself")
	}

	authClient := s.GetSvcConn().AuthServiceClient()
	validateMutual, err := authClient.CheckUserById(ctx, &authPB.DefaultRequest{
		UserId: req.UserId,
	})
	if err != nil {
		return nil, err
	}

	if validateMutual.Code == http.StatusNotFound {
		return nil, errors.New("User not found")
	}

	swipedToday, err := s.provider.GetSwipedMutualIdsToday(ctx, userId)
	if err != nil {
		return nil, err
	}

	maxCountData, err := authClient.GetUserMaxCount(ctx, &authPB.DefaultRequest{
		UserId: userId,
	})
	if err != nil {
		return nil, err
	}

	limit := int(maxCountData.GetMaxCount()) - len(swipedToday)
	if limit <= 0 {
		return nil, errors.New("Limit reached, can't swipe anymore today")
	}

	err = s.provider.Swipe(ctx, userId, req)
	if err != nil {
		return nil, err
	}

	return &pb.DefaultResponse{
		Error:   false,
		Code:    http.StatusOK,
		Message: "Success",
	}, nil
}
