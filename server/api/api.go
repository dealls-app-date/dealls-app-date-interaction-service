package api

import (
	"dealls-app-date-interaction-service/server/config"
	"dealls-app-date-interaction-service/server/db"
	manager "dealls-app-date-interaction-service/server/jwt"
	"log"
	"time"

	servicelogger "dealls-app-date-interaction-service/server/lib/service-logger"
	pb "dealls-app-date-interaction-service/server/pb"
	svc "dealls-app-date-interaction-service/server/services"
)

// Server represents the server implementation of the SW API.
type Server struct {
	provider  *db.Provider
	scvConn   *svc.ServiceConnection
	logger    *servicelogger.AddonsLogrus
	appConfig *config.Config
	manager   *manager.JWTManager

	pb.InteractionServiceServer
}

func New(
	provider *db.Provider,
	svcConn *svc.ServiceConnection,
	logger *servicelogger.AddonsLogrus,
	appConfig *config.Config,
) *Server {
	tokenRDuration, err := time.ParseDuration("30m")
	if err != nil {
		log.Panic(err)
	}

	return &Server{
		provider:  provider,
		scvConn:   svcConn,
		logger:    logger,
		appConfig: appConfig,
		manager:   manager.NewJWTManager(appConfig.AppKey, tokenRDuration, logger),
	}
}

func (s *Server) GetProvider() *db.Provider {
	return s.provider
}

func (s *Server) GetSvcConn() *svc.ServiceConnection {
	return s.scvConn
}

func (s *Server) GetLogger() *servicelogger.AddonsLogrus {
	return s.logger
}
