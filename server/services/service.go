package services

import (
	"fmt"
	"log"
	"os"

	authPB "dealls-app-date-interaction-service/server/lib/stubs/auth-service"

	"go.elastic.co/apm/module/apmgrpc/v2"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceConnection struct {
	AuthService        *grpc.ClientConn
	InteractionService *grpc.ClientConn
}

type ServiceOption struct {
	AuthService        string
	InteractionService string
}

func InitServicesConn(certFile string, options ServiceOption) *ServiceConnection {
	var err error
	var creds credentials.TransportCredentials
	if certFile != "" {
		creds, err = credentials.NewClientTLSFromFile(certFile, "")
		if err != nil {
			log.Panic(err)
		}
	} else {
		creds = insecure.NewCredentials()
	}
	var opts []grpc.DialOption
	opts = append(opts,
		grpc.WithTransportCredentials(creds),
		grpc.WithUnaryInterceptor(apmgrpc.NewUnaryClientInterceptor()),
		grpc.WithStreamInterceptor(apmgrpc.NewStreamClientInterceptor()),
	)

	services := &ServiceConnection{}

	// Interaction Service
	services.InteractionService, err = initGrpcClientConn(options.InteractionService, "Interaction Service .. ", opts...)
	if err != nil {
		log.Fatalf("%v", err)
		os.Exit(1)
		return nil
	}

	// Auth Service
	services.AuthService, err = initGrpcClientConn(options.AuthService, "Auth Service .. ", opts...)
	if err != nil {
		log.Fatalf("%v", err)
		os.Exit(1)
		return nil
	}

	return services
}

func (s *ServiceConnection) AuthServiceClient() authPB.AuthServiceClient {
	return authPB.NewAuthServiceClient(s.AuthService)
}

func initGrpcClientConn(address string, name string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	if address == "" {
		return nil, fmt.Errorf("%s address is empty", name)
	}

	conn, err := grpc.Dial(address, opts...)
	if err != nil {
		return nil, fmt.Errorf("failed connect to %s: %v", name, err)
	}

	log.Printf("[service - connection] %s State: %s", name, conn.GetState().String())
	log.Printf("[service - connection] %s Connected, on %s", name, address)

	return conn, nil
}

func (s *ServiceConnection) CloseAllServicesConn() {
	s.CloseAuthServiceConn()
	s.CloseInteractionConn()
}

func (s *ServiceConnection) CloseAuthServiceConn() error {
	return s.AuthService.Close()
}

func (s *ServiceConnection) CloseInteractionConn() error {
	return s.InteractionService.Close()
}
