package db

import (
	"context"
	"dealls-app-date-interaction-service/server/pb"
	"log"
	"time"

	"go.elastic.co/apm/v2"
)

func (p *Provider) GetSwipedMutualIdsToday(ctx context.Context, userId uint64) ([]*uint64, error) {
	span, _ := apm.StartSpan(ctx, "GetSwipedTodayCount", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	currentTime := time.Now()
	currentDate := currentTime.Format("2006-01-02")
	sqlStmt, stmtErr := p.GetDbSql().SqlDb.Prepare(`SELECT mutual_id FROM user_swipes WHERE user_id = $1 AND updated_at >= $2 AND updated_at <= $3`)
	if stmtErr != nil {
		log.Fatal(stmtErr)
		return nil, stmtErr
	}
	defer sqlStmt.Close()

	ctxTimeout, ctxCancel := context.WithTimeout(ctx, p.GetDbSql().GetTimeout())
	defer ctxCancel()

	result, err := sqlStmt.QueryContext(ctxTimeout, userId, currentDate+" 00:00:00", currentDate+" 23:59:59")
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	var mutualIds []*uint64
	for result.Next() {
		var mutualId uint64
		if err := result.Scan(&mutualId); err != nil {
			log.Fatal(err)
			return nil, err
		}

		mutualIds = append(mutualIds, &mutualId)
	}

	return mutualIds, nil
}

func (p *Provider) Swipe(ctx context.Context, currentUserId uint64, req *pb.SwipeRequest) error {
	span, _ := apm.StartSpan(ctx, "Swipe", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	currentTime := time.Now()
	currentDate := currentTime.Format("2006-01-02")
	query := `
		INSERT INTO user_swipes (user_id, mutual_id, is_like)
		VALUES ($1, $2, $3)
		ON CONFLICT (user_id, mutual_id)
		DO UPDATE SET mutual_id = EXCLUDED.mutual_id, is_like = EXCLUDED.is_like WHERE EXCLUDED.updated_at < $4`

	_, err := p.GetDbSql().SqlDb.Exec(query, currentUserId, req.UserId, req.IsLike, currentDate+" 00:00:00")
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}
