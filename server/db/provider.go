package db

import (
	"dealls-app-date-interaction-service/server/lib/database"
)

type Provider struct {
	dbSql *database.DbSql
}

func New(dbSql *database.DbSql) *Provider {
	return &Provider{
		dbSql: dbSql,
	}
}

func (p *Provider) GetDbSql() *database.DbSql {
	return p.dbSql
}
