package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strings"

	"dealls-app-date-interaction-service/server/api"
	"dealls-app-date-interaction-service/server/config"
	"dealls-app-date-interaction-service/server/db"
	servicelogger "dealls-app-date-interaction-service/server/lib/service-logger"
	pb "dealls-app-date-interaction-service/server/pb"
	svc "dealls-app-date-interaction-service/server/services"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
	"go.elastic.co/apm/module/apmgrpc/v2"
	"go.elastic.co/apm/module/apmhttp/v2"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
)

const serviceName = "dealls-app-date-interaction-service"
const defaultPort = 9090

var s *grpc.Server
var logger *servicelogger.AddonsLogrus
var appConfig *config.Config

func init() {
	appConfig = config.InitConfig(".env")
	logger = servicelogger.New(serviceName, appConfig)
}

func main() {
	app := cli.NewApp()
	app.Name = ""
	app.Commands = []cli.Command{
		grpcServerCmd(),
		gatewayServerCmd(),
		grpcGatewayServerCmd(),
	}

	if err := app.Run(os.Args); err != nil {
		fmt.Fprintf(os.Stderr, "error: %s", err.Error())
		os.Exit(1)
	}
}

func gatewayServerCmd() cli.Command {
	return cli.Command{
		Name:  "gw-server",
		Usage: "starts a Gateway server",
		Flags: []cli.Flag{
			cli.IntFlag{
				Name:  "port",
				Value: 3000,
			},
			cli.StringFlag{
				Name:  "grpc-endpoint",
				Value: ":" + fmt.Sprint(defaultPort),
				Usage: "the address of the running gRPC server to transcode to",
			},
		},
		Action: func(c *cli.Context) error {
			port, grpcEndpoint := c.Int("port"), c.String("grpc-endpoint")

			go func() {
				if err := httpGatewayServer(port, grpcEndpoint); err != nil {
					logger.Fatalf("failed JSON Gateway serve: %v", err)
				}
			}()

			// Wait for Control C to exit
			ch := make(chan os.Signal, 1)
			signal.Notify(ch, os.Interrupt)
			// Block until a signal is received
			<-ch

			logger.Info("JSON Gateway server stopped")

			return nil
		},
	}
}

func grpcServerCmd() cli.Command {
	return cli.Command{
		Name:  "grpc-server",
		Usage: "starts a gRPC server",
		Flags: []cli.Flag{
			cli.IntFlag{
				Name:  "port",
				Value: defaultPort,
			},
		},
		Action: func(c *cli.Context) error {
			port := c.Int("port")
			ch := make(chan os.Signal, 1)
			signal.Notify(ch, os.Interrupt)

			startDBConnection()

			go func() {
				if err := grpcServer(port, ch); err != nil {
					logger.Fatalf("failed RPC serve: %v", err)
				}
			}()

			// Wait for Control C to exit
			// Block until a signal is received
			<-ch

			closeDBConnections()

			logger.Info("Stopping RPC server")
			s.Stop()
			logger.Info("RPC server stopped")
			return nil
		},
	}
}

func grpcGatewayServerCmd() cli.Command {
	return cli.Command{
		Name:  "grpc-gw-server",
		Usage: "Starts gRPC and Gateway server",
		Flags: []cli.Flag{
			cli.IntFlag{
				Name:  "port1",
				Value: defaultPort,
			},
			cli.IntFlag{
				Name:  "port2",
				Value: 3000,
			},
			cli.StringFlag{
				Name:  "grpc-endpoint",
				Value: ":" + fmt.Sprint(defaultPort),
				Usage: "the address of the running gRPC server to transcode to",
			},
		},
		Action: func(c *cli.Context) error {
			rpcPort, httpPort, grpcEndpoint := c.Int("port1"), c.Int("port2"), c.String("grpc-endpoint")

			// Wait for Control C to exit
			ch := make(chan os.Signal, 1)
			signal.Notify(ch, os.Interrupt)
			startDBConnection()

			go func() {
				if err := grpcServer(rpcPort, ch); err != nil {
					logger.Fatalf("failed RPC serve: %v", err)
				}
			}()

			go func() {
				if err := httpGatewayServer(httpPort, grpcEndpoint); err != nil {
					logger.Fatalf("failed JSON Gateway serve: %v", err)
				}
			}()

			// Block until a signal is received
			<-ch

			closeDBConnections()
			logger.Info("Stopping RPC server")
			s.Stop()
			logger.Info("RPC server stopped")
			logger.Info("JSON Gateway server stopped")

			return nil
		},
	}
}

func grpcServer(port int, ch chan os.Signal) error {
	// RPC
	logger.Infof("Starting %s Service ................", serviceName)
	logger.Infof("Starting RPC server on port %d...", port)
	list, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return err
	}

	svcConn := svc.InitServicesConn("", svc.ServiceOption{
		AuthService:        appConfig.AuthService,
		InteractionService: appConfig.InteractionService,
	})
	defer svcConn.CloseAllServicesConn()

	dbProvider := db.New(dbSql)

	apiServer := api.New(
		dbProvider,
		svcConn,
		logger,
		appConfig,
	)

	s = grpc.NewServer()
	pb.RegisterInteractionServiceServer(s, apiServer)
	grpc_health_v1.RegisterHealthServer(s, health.NewServer())

	return s.Serve(list)
}

func httpGatewayServer(port int, grpcEndpoint string) error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Connect to the GRPC server
	conn, err := grpc.Dial(
		grpcEndpoint,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithUnaryInterceptor(apmgrpc.NewUnaryClientInterceptor()),
		grpc.WithStreamInterceptor(apmgrpc.NewStreamClientInterceptor()))
	if err != nil {
		return err
	}
	defer conn.Close()

	rmux := runtime.NewServeMux(
		runtime.WithErrorHandler(CustomHTTPError),
		runtime.WithForwardResponseOption(httpResponseModifier),
	)

	client := pb.NewInteractionServiceClient(conn)
	err = pb.RegisterInteractionServiceHandlerClient(ctx, rmux, client)
	if err != nil {
		return err
	}

	// Serve the swagger-ui and swagger file
	mux := http.NewServeMux()
	mux.Handle("/", rmux)

	mux.HandleFunc("/api/interactions/docs/swagger.json", serveSwagger)
	fs := http.FileServer(http.Dir(appConfig.SwaggerPath + "swagger-ui"))
	mux.Handle("/api/interactions/docs/", http.StripPrefix("/api/interactions/docs/", fs))

	// Start
	logger.Infof("Starting JSON Gateway server on port %d...", port)

	return http.ListenAndServe(fmt.Sprintf(":%d", port), apmhttp.Wrap(setHeaders(mux)))
}

func serveSwagger(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, appConfig.SwaggerPath+"swagger.json")
}

func allowedOrigin(origin string) bool {
	if stringInSlice(viper.GetString("cors"), appConfig.CorsAllowedOrigins) {
		return true
	}
	if matched, _ := regexp.MatchString(viper.GetString("cors"), origin); matched {
		return true
	}
	return false
}

func setHeaders(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Strict-Transport-Security", "max-age=31536000")

		if allowedOrigin(r.Header.Get("Origin")) {
			// w.Header().Set("Content-Security-Policy", "default-src 'self';img-src data: https:;object-src 'none'; upgrade-insecure-requests;")
			w.Header().Set("Access-Control-Allow-Origin", strings.Join(appConfig.CorsAllowedOrigins, ", "))
			w.Header().Set("Access-Control-Allow-Methods", strings.Join(appConfig.CorsAllowedMethods, ", "))
			w.Header().Set("Access-Control-Allow-Headers", strings.Join(appConfig.CorsAllowedHeaders, ", "))
		}
		if r.Method == "OPTIONS" {
			return
		}
		h.ServeHTTP(w, r)
	})
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
