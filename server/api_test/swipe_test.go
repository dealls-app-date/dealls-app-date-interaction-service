package api_test

import (
	"context"
	"dealls-app-date-interaction-service/server/api"
	"dealls-app-date-interaction-service/server/config"
	"dealls-app-date-interaction-service/server/db"
	servicelogger "dealls-app-date-interaction-service/server/lib/service-logger"
	authPB "dealls-app-date-interaction-service/server/lib/stubs/auth-service"
	pb "dealls-app-date-interaction-service/server/pb"
	svc "dealls-app-date-interaction-service/server/services"
	"net/http"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/metadata"
)

var logger *servicelogger.AddonsLogrus

const serviceName = "dealls-app-date-interaction-service-test"

func TestGetSwipeProfile(t *testing.T) {
	// Ensure that the database connection is started before the tests
	startDBConnection()
	defer closeDBConnections()

	// Mock your dependencies such as config, logger, and service connection
	appConfig := config.InitConfig("../../.env")
	mockLogger := servicelogger.New(serviceName, appConfig)
	mockServiceConnection := svc.InitServicesConn("", svc.ServiceOption{
		AuthService:        appConfig.AuthService,
		InteractionService: appConfig.InteractionService,
	})

	// Create a new server instance
	server := api.New(db.New(dbSql), mockServiceConnection, mockLogger, appConfig)
	logrus.Infof("secret key %v", appConfig.AuthService)
	authClient := server.GetSvcConn().AuthServiceClient()

	// Create a context
	ctx := context.Background()
	login, err := authClient.Login(ctx, &authPB.LoginRequest{
		Email:    "andriamirul@gmail.com",
		Password: "Coba12345!",
	})
	assert.NoError(t, err)

	var newCtx context.Context
	md, _ := metadata.FromIncomingContext(ctx)
	if md == nil {
		md = metadata.New(map[string]string{})
	}
	md.Append("authorization", "Bearer "+login.AccessToken)
	newCtx = metadata.NewIncomingContext(ctx, md)

	resp, err := server.GetSwipeProfile(newCtx, &pb.Empty{})
	assert.NoError(t, err)
	assert.NotNil(t, resp)
	assert.False(t, resp.Error)
	assert.Equal(t, uint32(http.StatusOK), resp.Code)
}

func TestSwipe(t *testing.T) {
	// Ensure that the database connection is started before the tests
	startDBConnection()
	defer closeDBConnections()

	// Mock your dependencies such as config, logger, and service connection
	appConfig := config.InitConfig("../../.env")
	mockLogger := servicelogger.New(serviceName, appConfig)
	mockServiceConnection := svc.InitServicesConn("", svc.ServiceOption{
		AuthService:        appConfig.AuthService,
		InteractionService: appConfig.InteractionService,
	})

	// Create a new server instance
	server := api.New(db.New(dbSql), mockServiceConnection, mockLogger, appConfig)
	logrus.Infof("secret key %v", appConfig.AuthService)
	authClient := server.GetSvcConn().AuthServiceClient()

	ctx := context.Background()
	login, err := authClient.Login(ctx, &authPB.LoginRequest{
		Email:    "andriamirul@gmail.com",
		Password: "Coba12345!",
	})
	assert.NoError(t, err)

	var newCtx context.Context
	md, _ := metadata.FromIncomingContext(ctx)
	if md == nil {
		md = metadata.New(map[string]string{})
	}
	md.Append("authorization", "Bearer "+login.AccessToken)
	newCtx = metadata.NewIncomingContext(ctx, md)

	// Create a mock SwipeRequest
	req := &pb.SwipeRequest{
		UserId: 24,
		IsLike: true,
	}

	// Call the Swipe function
	resp, err := server.Swipe(newCtx, req)
	assert.NoError(t, err)
	assert.NotNil(t, resp)
	assert.False(t, resp.Error)
	assert.Equal(t, uint32(http.StatusOK), resp.Code)
}
